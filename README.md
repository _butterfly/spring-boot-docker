## Gitlab CI/CD valiables
- **NVDAPIKEY**: National Vulnerrability Database API Key
    https://nvd.nist.gov/general/news/API-Key-Announcement
- **CI_REPO_USER**: Artifactory User
- **CI_REPO_PASSWORD**: Artifactory Password
- **DEVELOP_WEBSITE_URL**: Web URL on Development environment
- **PROD_WEBSITE_URLL**: Web URL on Production environment

## What is it?
This source code is an Spring Boot web application (mvc + thymeleaf).
 
Tested with
* Docker 19.03
* Ubuntu 19
* Java 8 or Java 11
* Spring Boot 2.2.4.RELEASE
* Maven

For explanation, please visit this article - [Docker and Spring Boot](https://mkyong.com/docker/docker-spring-boot-examples/)

## How to run this?
```bash
$ git clone https://gitlab.yipintsoi.com/example/spring-docker-examnple.git
cd spring-docker-examnple
mvn clean package
$ java -jar target/spring-boot-web.jar

  access http://localhost:8080

//dockerize

// create a docker image
$ sudo docker build -t spring-boot:1.0 .
// run it
$ sudo docker run -d -p 8080:8080 -t spring-boot:1.0

  access http://localhost:8080

```

[WiKi](../../wikis/AsciiDoc-Example) (Example)
